## [1.5.2](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.5.1...v1.5.2) (2021-10-18)


### Bug Fixes

* **onboarding:** added missing chmod in installer ([3b7fa32](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/3b7fa327c6082258ceb1ba3354a86ae67f67c0a7))

## [1.5.1](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.5.0...v1.5.1) (2021-10-17)


### Bug Fixes

* **onboarding:** added installer docs ([1257325](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/1257325b15562768b2ec51ae3d4e81be0c827db1))

# [1.5.0](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.4.1...v1.5.0) (2021-10-17)


### Features

* changed name ([7f6e0e6](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/7f6e0e66b1176cfb5a39aeb6855a6753d89a1092))

## [1.4.1](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.4.0...v1.4.1) (2021-10-17)


### Bug Fixes

* **onboarding:** added quick start installation method ([abd91a9](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/abd91a9d79a35fd6f952973b6b6d1fd3432cf6af))

# [1.4.0](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.3.0...v1.4.0) (2021-10-17)


### Features

* added script for easy installation ([53fef1f](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/53fef1f306cd18d9458a174eceef4ca4e8e00b09))

# [1.3.0](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.2.5...v1.3.0) (2021-10-16)


### Features

* **onboarding:** improved onboarding docs ([7dccd59](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/7dccd59a303be7a2c75a5496417186b471ff2029))

## [1.2.5](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.2.4...v1.2.5) (2021-10-02)


### Bug Fixes

* **docs:** corrected defaults ([2d957c8](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/2d957c8815a79d3d8b33863a35246d9d6fc1a15f))

## [1.2.4](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.2.3...v1.2.4) (2021-10-02)


### Bug Fixes

* **helm:** corrected image registry path ([b9f8370](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/b9f837093b92322c9b853776cbf915c8487eaa01))

## [1.2.3](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.2.2...v1.2.3) (2021-10-02)


### Bug Fixes

* **docs:** updated cloudstack docs with docker-for-desktop example ([bf4ec56](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/bf4ec564062a0f4983573ab3bdfeb9d5cf56ee3d))

## [1.2.2](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.2.1...v1.2.2) (2021-09-10)


### Bug Fixes

* renamed acs to cloudstack ([8426c48](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/8426c485331cbf4c1a20299ec9c36a1c7751c8fa))

## [1.2.1](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.2.0...v1.2.1) (2021-09-01)


### Bug Fixes

* corrected install tutorial ([f0ac351](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/f0ac351ea1b7ddc53f9c234a43a1f23b44396753))

# [1.2.0](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.1.0...v1.2.0) (2021-08-31)


### Features

* updated docs ([1925803](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/19258039633539395b2398200681c5b7cc7d4925))

# [1.1.0](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.10...v1.1.0) (2021-08-31)


### Features

* updated docs ([28f1977](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/28f1977e83bcb82b007ca0649f8c3493b8c47778))

## [1.0.10](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.9...v1.0.10) (2021-08-31)


### Bug Fixes

* stuff ([3673c7e](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/3673c7eeabde74bc45b9ef972f519ac6859df00f))

## [1.0.9](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.8...v1.0.9) (2021-08-31)


### Bug Fixes

* ci ([16d8f5d](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/16d8f5d92ae50f1d10176ec16beae91d69688e30))

## [1.0.8](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.7...v1.0.8) (2021-08-31)


### Bug Fixes

* stuff ([de11dba](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/de11dbace32ae5e8996ff5c29b6fe8f47bd0594c))

## [1.0.7](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.6...v1.0.7) (2021-08-31)


### Bug Fixes

* stuff ([e433599](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/e433599d018f67aba6afdb16e8ade1f8651df187))

## [1.0.6](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.5...v1.0.6) (2021-08-31)


### Bug Fixes

* stuff ([38dbe39](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/38dbe390ee8932f5da5c21c4cccebcfd6c439be1))

## [1.0.5](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.4...v1.0.5) (2021-08-31)


### Bug Fixes

* stuff ([6ab447b](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/6ab447b4c27c23c1801eb7e8e74469dce918dbe6))

## [1.0.4](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.3...v1.0.4) (2021-08-31)


### Bug Fixes

* stuff ([93b4a44](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/93b4a44c6396ce94b9c89d507fe11f57810dca3f))

## [1.0.3](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.2...v1.0.3) (2021-08-31)


### Bug Fixes

* stuff ([a216dbc](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/a216dbc4dd9b4ee35bf4aa5f97c0d89b9d0b26eb))

## [1.0.2](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.1...v1.0.2) (2021-08-31)


### Bug Fixes

* stuff ([8c4660c](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/8c4660c77559eb8487f13b85957b076dc82ba478))

## [1.0.1](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/compare/v1.0.0...v1.0.1) (2021-08-31)


### Bug Fixes

* stuff ([75df8ca](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/75df8ca9f8e091f92ce0dcbe500a90377828de92))

# 1.0.0 (2021-08-31)


### Bug Fixes

* docker stuff ([c8055ac](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/c8055ac70c07972bc1ad501d624c74c7a96dda06))
* dockerfile ([824e517](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/824e51772c3503976b6b6cdd00a09edcd5fa1124))
* dockerfile ([55c9e5a](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/55c9e5a88bc65c15ca638b9f674d3ee827b83546))
* dockerfile ([4bea792](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/4bea7928f0f1374a5ffd6758814e235ae8d3fa4d))


### Features

* initial commit ([df26ffc](https://gitlab.com/ayedocloudsolutions/cloudstack/docs/commit/df26ffcb271fff8829911f9261e5819a4fd108eb))
