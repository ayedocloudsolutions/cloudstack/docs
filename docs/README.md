---
title: Getting Started
description: A fully integrated DevOps platform providing many of the most relevant features of modern cloud providers to developers for self-hosting
---

# Getting Started

!!! danger "beta-grade software"
    If you're interested in working with the Cloudstack, please [reach out](https://www.ayedo.de)

Cloudstack is a composable, Kubernetes-based container platform. You can start with a single node for development and scale to a production-grade system along the way. Cloudstack can [create infrastructure](IaaS.md) and [setup Kubernetes](KaaS.md) for you, too. It's built around industry best practices and mostly unopinionated, allowing you to deploy your cloud native workloads in the way that fits best for you while giving you a concise framework for painless operations. Cloudstack comes with a suite of well-known and tightly integrated tools that you can leverage to support your software development process. It's designed to work with standard Kubernetes clusters and thus enables a common workflow for development and production deployments.

## Installation

### Automated installer

Using the [Cloudstack installer](Installer.md) automates the process of downloading and moving to your `$PATH`. The installer automatically detects your operating system and architecture.

=== "curl"

    **real** quick:

    ```bash
    curl https://docs.cloudstack.one/get-cloudstack.sh | bash
    ```

    a bit safer:

    ```bash
    curl -fsSL -o get-cloudstack.sh https://docs.cloudstack.one/get-cloudstack.sh
    chmod 0700 get-cloudstack.sh
    # Optionally: less get-cloudstack.sh
    ./get-cloudstack.sh
    ```

=== "wget"

     **real** quick:

    ```bash
    wget -qO- https://docs.cloudstack.one/get-cloudstack.sh | bash
    ```

    a bit safer:

    ```bash
    wget -q -O get-cloudstack.sh https://docs.cloudstack.one/get-cloudstack.sh
    chmod 0700 get-cloudstack.sh
    # Optionally: less get-cloudstack.sh
    ./get-cloudstack.sh
    ```

### Manual Download

Install the [CLI](https://gitlab.com/ayedocloudsolutions/cloudstack/cli) by following the steps for your platform below:

=== "Linux"

    ``` bash
    curl -fsSLo ./cloudstack https://s3.ayedo.dev/packages/cloudstack/latest/cloudstack-linux-amd64
    chmod +x cloudstack
    ./cloudstack version
    ```

=== "Linux (ARM)"

    ``` bash
    curl -fsSLo ./cloudstack https://s3.ayedo.dev/packages/cloudstack/latest/cloudstack-linux-arm64
    chmod +x cloudstack
    ./cloudstack version
    ```

=== "macOS"

    ``` bash
    curl -fsSLo ./cloudstack https://s3.ayedo.dev/packages/cloudstack/latest/cloudstack-darwin-amd64
    chmod +x cloudstack
    ./cloudstack version
    ```

=== "macOS (M1)"

    ``` bash
    curl -fsSLo ./cloudstack https://s3.ayedo.dev/packages/cloudstack/latest/cloudstack-darwin-arm64
    chmod +x cloudstack
    ./cloudstack version
    ```

=== "Windows"

    ``` bash
    curl -fsSLo ./cloudstack.exe https://s3.ayedo.dev/packages/cloudstack/latest/cloudstack-windows-amd64.exe
    chmod +x cloudstack.exe
    ./cloudstack.exe version
    ```

## Quick start

After you've installed the CLI, you can bootstrap a new stack. Let's call it `my-cloudstack` and create the corresponding **context directory**.

!!! note
    The **context directory** is where the configuration (the so called **Stackfile**) and lifecycle artifacts (e.g. TLS certifcates and SSH keys) of a stack are saved. You can set a custom context directory with `--context`. By default, the context directory is set to your current working directory.

Running `cloudstack init` will get you started with an example config that works with **Docker for Desktop** out of the box.

```bash
mkdir my-cloudstack
cd my-cloudstack
cloudstack init
```

!!! note
    Instead of creating and entering the context directory manually, you can initialize a new stack from anywhere and have the directory created automatically by using the following command: `cloudstack --context $PWD/my-cloudstack init`

You can install your new stack running the following command:

```bash
cloudstack install --pull
```

!!! note
    `--pull` downloads the latest Cloudstack image from GitLab's [container registry](https://gitlab.com/ayedocloudsolutions/cloudstack/cloudstack/container_registry/2239893). You can use `--image-ref` to specify your own image (e.g. if you forked Cloudstack). By default, the CLI will use the `latest` tag of your image. Use `--image-version` to specify a different tag.

!!! note
    In case you're not using the CLI from within the context directory, you can specify the context directory manually: `cloudstack --context $PWD/my-cloudstack install --pull`.

!!! note
    The CLI tries to automatically discover the Kubernetes cluster to work with by detecting a **kubeconfig** file. By default, it looks for a `kubeconfig.yml` in the context directory. This file is automatically created for you if you use Cloudstack's [IaaS component](IaaS.md). If it doesn't exist, the CLI tries to fall back to `$HOME/.kube/config` and the `KUBECONFIG` environment variable in this order. If no kubeconfig can be found, the parts of the stack that interface with your Kubernetes cluster won't work.

    You can manually specify a kubeconfig file using `--kubeconfig` which has precedence over the automatic discovery process.

After installation, you can access your stack under the following URLs:

- [Grafana](http://grafana.my.cloudstack.one)
- [ArgoCD](http://argocd.my.cloudstack.one)
- [Portainer](http://portainer.my.cloudstack.one)

## Customization

## Troubleshooting

## Configuration

Cloudstack configuration is defined in a `Stackfile` in the context directory.

The defaults can be found [here](https://gitlab.com/ayedocloudsolutions/acs/-/blob/main/defaults.yml).