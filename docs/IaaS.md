# IaaS

![AKI](./img/AKI.svg)

The IaaS component of the Cloudstack manages infrastructure for Kubernetes clusters in the cloud or on supported on-premise providers.

## Get started

### Configuration

In your context directory, create your configuration file:

```bash
edit ~/.acs/mycluster.example.com/acs.yml
```

Setup your cluster configuration:

```yaml
# ~/.acs/mycluster.example.com/acs.yml
stack:
  name: mycluster.example.com
iaas:
  provider: "hcloud"
hcloud:
  token: "YOUR_HCLOUD_TOKEN"
  master:
    count: 1
    type: "cx31"
    location: "fsn1"
  node:
    count: 3
    type: "cx31"
    location: "fsn1"
  os_image: "ubuntu-20.04"
```

> INFO: You can see the defaults [here](https://gitlab.com/ayedocloudsolutions/aki/-/blob/main/defaults.yml).

### Use with Docker

```bash
docker run -e "STACK_DIR=/root/.acs/mycluster.example.com" -v ~/.acs/mycluster.example.com:/root/.acs/mycluster.example.com registry.gitlab.com/ayedocloudsolutions/aki ansible-playbook install.yml
```

### Use manually

```bash
export STACK_DIR=~/.acs/mycluster.example.com
ansible-playbook install.yml
```

Upon completion, AKI will save your Terraform files and an [AKE-compatible inventory](#) in `~/.acs/mycluster.example.com`. Use these files to make changes to your infrastructure with Terraform or install [A Kubernetes Engine](#).

## Configuration

Default configuration options can be found in [defaults.yml](defaults.yml). This file will be loaded by `install.yml`. Additional configuration options can be set in multiple ways:

- creating user configuration in `~/.acs/mycluster.example.com/acs.yml`. AKI picks up user-config automatically from there.

### Configuration options

| Configuration Option  |                                    Description                                    |                   Default |
| --------------------- | :-------------------------------------------------------------------------------: | ------------------------: |
| `ssh_public_key`      | Path of your public ssh key (will be added to the infrastructure for easy access) |       `~/.ssh/id_rsa.pub` |
| `in_ci`               |                               Set to True if in CI                                |                   `False` |
| `stack_dir`           |                               Location of the stack                               | lookup EnvVar `STACK_DIR` |
| `stack.name`          |                                 Name of the stack                                 |                       `-` |
| `iaas.provider`       |                             The provider to work with                             |                  `hcloud` |
| `hcloud.token`        |                              HETZNER Cloud API Token                              |                       `-` |
| `hcloud.master.count` |                         Number of masters in the cluster                          |                       `1` |
| `hcloud.master.type`  |                                  Size of the VM                                   |                    `cx31` |

| `hcloud.master.location` | The location of the VM (datacenter)      | `fsn1` |
| `hcloud.node.count` | Number of nodes in the cluster      |`3` |
| `hcloud.node.type` | Size of the VM      |`cx31` |
| `hcloud.node.location` | Location of the VM (datacenter)      |`fsn1` |
| `hcloud.os_image` | OS Image of the VM      |`ubuntu-20.04` |

### Azure AKS

For Azure AKS, you need Service Principal Credentials that you can obtain like this:

```bash
az ad sp create-for-rbac --skip-assignment
```

This results in the following output:

```json
{
  "appId": "b4f1c486-asdasd-asdasd-b617-asdasd",
  "displayName": "azure-cli-2021-08-23-14-47-19",
  "name": "b4f1c486-adsads-4d2b-adsads-asdads",
  "password": "asdads-asdasd",
  "tenant": "asdasd-de3c-48b7-a724-asdasd"
}
```

The ACS related-config looks like this:

```yaml
iaas:
  provider: "azure_aks"
azure_aks:
  serviceprincipal:
    client:
      id: "b4f1c486-asdasd-asdasd-b617-asdasd"
      secret: "asdads-asdasd"
```


## Development

### Build Docker image

```bash
docker buildx build --platform linux/amd64 -t aki .
```

### Use AKI in Docker

```bash
docker run -v ~/.ssh:/root/.ssh -v ~/.acs/mycluster.example.com:/root/.acs/mycluster.example.com -e "STACKDIR=/root/.acs/mycluster.example.com" -v $PWD:/ansible aki ansible-playbook install.yml -e "stack=mycluster" -v
```

## Versioning

We use SemVer for versioning. For the versions available see the tags on this repository.

## Authors

- Fabian Peter
- Jan Stauder

See also the list of contributors who participated in this project.

## License

A Kubernetes Infrastructure is [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) licensed.

## Disclaimer

This software is maintained and commercially supported by [p3r.](https://www.p3r.one). You can reach us here:

- [Web](https://www.p3r.one)
- [Slack](https://join.slack.com/t/wearep3r/shared_invite/zt-d9ao21f9-pb70o46~82P~gxDTNy_JWw)
- [GitLab](https://gitlab.com/p3r.one)
- [GitHub](https://github.com/wearep3r/)
- [LinkedIn](https://www.linkedin.com/company/wearep3r)
