# KaaS

![AKE](./img/AKE.svg)

The [Ayedo Kubernetes Engine](#) ([AKE](#)) builds Kubernetes clusters from scratch on VMs or bare-metal machines.

It's Ansible-based and allows you to deploy Kubernetes clusters on any machine with a supported OS (Ubuntu, Debian, Centos) and architecture (x64, armhf, arm64) that you can access via SSH.

Depending on the provider of your machines (bare-metal or a supported cloud-provider), additional enhancements like CSI-integration are available.

AKE is a component of the [Ayedo Cloud Stack](https://www.ayedo.de/ayedo-cloud-stack) ([ACS](https://www.ayedo.de/ayedo-cloud-stack)) and uses a common directory to store output and configuration, the so called `$acs_dir` which defaults to `~/.acs`.

ACS works with so called **stacks**. As such, each installation of the Ayedo Kubernetes Engine requires a `$stack`, just like HELM. All of AKE's [output](#output) will be saved to the so called `$stack_dir` which defaults to `$acs_dir/$stack`.

## Required software

- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) >= 2.10
- [Python OpenShift](https://pypi.org/project/openshift/)
- [Helm](https://helm.sh)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)

### Install with pip

```bash
pip3 install -r requirements.txt
```

## Get started

We will work with a new installation that we'll call `mycluster.example.com`. This gives us the following base-config:

- `stack=mycluster.example.com`
- `stack_dir=~/.acs/mycluster.example.com`

> INFO: AKE expects your configuration and inventory to be in this directory

### Create your inventory

AKE requires a valid Ansible inventory to work. In this inventory, all masters and nodes of the cluster need to be defined.

> INFO: Use the [Ayedo Kubernetes Infrastructure](https://gitlab.com/ayedocloudsolutions/aki) (short: AKI) to build an inventory in the cloud or with one of the supported on-premise providers

The composition of the cluster is out of scope for the AKE. AKE expects a minimum of 1 master node.

Create a directory (the `$stack_dir`) for your new cluster:

```bash
mkdir -p ~/.acs/mycluster.example.com
```

Create an inventory file for your new cluster:

```bash
edit ~/.acs/mycluster.example.com/inventory.yml
```

> NOTE: this must be a valid Ansible inventory; it doesn't matter if it's YAML, JSON or INI format

Setup your nodes:

```yaml
# ~/.acs/mycluster.example.com/inventory.yml
all:
  hosts:
    master-0:
      ansible_host: 1.2.3.4
    node-0:
      ansible_host: 4.5.6.7
    node-1:
      ansible_host: 5.6.7.8
  children:
    master:
      hosts:
        master-0:
    node:
      hosts:
        node-0:        
        node-1:
    k3s_cluster:
      children:
        master:
        node:
```

> NOTE: this is pure Ansible using the defaults. Depending on your setup your inventory might look more complex or you'll need to specify additional configuration for the SSH user or a specific SSH key file to use.

### Create your configuration files

In your `$stack_dir`, create a vars-file:

```bash
edit ~/.acs/mycluster.example.com/acs.yml
```

Setup your cluster configuration:

```yaml
# ~/.acs/mycluster.example.com/acs.yml
csi:
  provider: longhorn
```

> INFO: You can see the defaults [here](https://gitlab.com/ayedocloudsolutions/ake/-/blob/main/defaults.yml).

### Use with Docker

```bash
docker run -e "STACK_DIR=/root/.acs/mycluster.example.com" -v ~/.acs/mycluster.example.com:/root/.acs/mycluster.example.com registry.gitlab.com/ayedocloudsolutions/ake ansible-playbook -i /root/.acs/mycluster.example.com/inventory.yml install.yml
```

### Use manually

```bash
export STACK_DIR=~/.acs/mycluster.example.com
ansible-playbook install.yml -i ~/.acs/mycluster/ake/inventory.yml
```

Upon completion, AKE will save your Kubernetes credentials in `~/.acs/mycluster.example.com/kubeconfig.yml`. Use this file to connect to the Kubernetes cluster.

## Configuration

Default configuration options can be found in [defaults.yml](defaults.yml). This file will be loaded by `install.yml`. Additional configuration options can be set in multiple ways:

- creating user configuration in `~/.acs/mycluster.example.com/acs.yml`. AKE picks up user-config automatically from there.

### Configuration options

| Configuration Option | Description | Default |
| -------------------- | :---------: | ------: |: |
| `stack_dir`             |                Location of the stack                 |                                                                                   lookup EnvVar `STACK_DIR` |
| `stack.name`            |                  Name of the stack                   |                                                                                                         `-` |
| `k3s_extra_agent_args`  |        Additional arguments for the k3s agent        |                                                                                                         `-` |
| `k3s_extra_server_args` |       Additional arguments for the k3s server        | `--disable traefik,local-storage --disable-cloud-controller --node-taint CriticalAddonsOnly=true:NoExecute` |
| `k3s_version`           |                     k3s version                      |                                                                                              `v1.21.1+k3s1` |
| `k3s_token_`            | k3s token for encryption of inter-node communication |                                                                                        `mysupersecrettoken` |
| `csi.provider`          |   Enable/Disable CSI integration for this provider   |                                                                                                  `longhorn` |
| `hcloud.token`          |               HETZNER Cloud API token                |                                                                                                         `-` |

| `systemd_dir` | The directory on the remote nodes where k3s service files should go to      | `/etc/systemd/system` |
| `ssh_public_keys` | Additional public ssh-keys to add to the nodes      |`-` |

## Issues / Troubleshooting

- `+[__NSCFConstantString initialize] may have been in progress in another thread when fork() was called. We cannot safely call it or ignore it in the fork() child process. Crashing instead.`:  https://stackoverflow.com/questions/50168647/multiprocessing-causes-python-to-crash-and-gives-an-error-may-have-been-in-progr

## Versioning

We use SemVer for versioning. For the versions available see the tags on this repository.

## Authors

- Fabian Peter
- Jan Stauder

See also the list of contributors who participated in this project.

## License

A Kubernetes Engine is [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) licensed.

## Disclaimer

This software is maintained and commercially supported by [p3r.](https://www.p3r.one). You can reach us here:

- [Web](https://www.p3r.one)
- [Slack](https://join.slack.com/t/wearep3r/shared_invite/zt-d9ao21f9-pb70o46~82P~gxDTNy_JWw)
- [GitLab](https://gitlab.com/p3r.one)
- [GitHub](https://github.com/wearep3r/)
- [LinkedIn](https://www.linkedin.com/company/wearep3r)
